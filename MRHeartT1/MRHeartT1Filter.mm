//
//  MRHeartT1Filter.m
//  MRHeartT1
//
//  Copyright (c) 2015 Konrad_Werys. All rights reserved.
//

#import "MRHeartT1Filter.h"
#include "MRHeartCalcT1.h"

@implementation MRHeartT1Filter

- (void) initPlugin
{
}

- (long) filterImage:(NSString*) menuName
{
    [self addShmolliColormap];
    [self addMolliColormap];
    
    NSArray         *pixList = [viewerController pixList: 0];
    DCMPix          *curPix = [pixList objectAtIndex: 0];
    int pixWidth            = curPix.pwidth;
    int pixHeight           = curPix.pheight;
    
    ViewerController *T1Viewer = [self newViewerControlerSimilarTo: curPix withNSlices: 1];
    ViewerController *R2Viewer = [self newViewerControlerSimilarTo: curPix withNSlices: 4];
    
    int NImages = pixList.count;
    
    vnl_index_sort<double,int> mysort;
    vnl_vector<double> invTimes(NImages);
    vnl_vector<double> ysignal(NImages);
    vnl_vector<int> myindices(NImages);
    
    for (int i=0; i<NImages; i++) {myindices[i]=i;}
    
    NSString *dcmTagContent;
    DCMObject *dcmObj;
    DCMAttributeTag *dcmTagName;
    NSScanner *objScanner;
    
    double temp;
    for (int iPix=0; iPix<pixList.count; iPix++){
        curPix = [pixList objectAtIndex: iPix];
        dcmObj = [DCMObject objectWithContentsOfFile:[curPix sourceFile] decodingPixelData:NO];
        dcmTagName = [DCMAttributeTag tagWithName:@"ImageComments"];
        dcmTagContent = [[[dcmObj attributeForTag:dcmTagName] value] description];
        objScanner = [NSScanner scannerWithString:dcmTagContent];
        [objScanner scanString:@"TIeff " intoString:nil];
        [objScanner scanString:@" ms" intoString:nil];
        [objScanner scanDouble:&temp];
        invTimes[iPix] = temp;
    }
    
    mysort.vector_sort_in_place(invTimes, myindices);
    vcl_cout << invTimes << vcl_endl;
    
    float *T1ImagePtr =     [[[T1Viewer pixList:0] objectAtIndex:0] fImage];
    float *R2ImagePtr =     [[[R2Viewer pixList:0] objectAtIndex:0] fImage];
    float *AImagePtr =      [[[R2Viewer pixList:0] objectAtIndex:1] fImage];
    float *BImagePtr =      [[[R2Viewer pixList:0] objectAtIndex:2] fImage];
    float *T1starImagePtr = [[[R2Viewer pixList:0] objectAtIndex:3] fImage];
    
    float *images[NImages];
    for (int iPix=0; iPix<NImages; iPix++){
        //here indexing is very very important!!!
        images[iPix] = [[pixList objectAtIndex: myindices[iPix]] fImage];
    }
    
    id waitWindow = [viewerController startWaitWindow:@"Wait, calculating T1 map ..."];
    
    struct timeval start, end;
    gettimeofday(&start, NULL);
    
    myT1calcPolarityImage(invTimes, pixWidth, pixHeight, images, AImagePtr, BImagePtr, T1starImagePtr, R2ImagePtr, T1ImagePtr);
    
    gettimeofday(&end, NULL);
    
    float delta = ((end.tv_sec - start.tv_sec) * 1000000u + end.tv_usec - start.tv_usec) / 1.e6;
    NSLog(@"T1 map calculated in: %f",delta);
    
    [viewerController endWaitWindow: waitWindow];
    
    [T1Viewer ApplyCLUTString:@"ShMolli_CLUT"];
    [T1Viewer setWL:2048 WW:4096];
    [T1Viewer needsDisplayUpdate];
    [R2Viewer setWL:3900 WW:200];
    [R2Viewer needsDisplayUpdate];
    return 0;
}

- (ViewerController*) newViewerControlerSimilarTo: (DCMPix*) curPix withNSlices: (int)nSlices{
    int pixWidth            = curPix.pwidth;
    int pixHeight           = curPix.pheight;
    float pixelSpacingX     = curPix.pixelSpacingX;
    float pixelSpacingY     = curPix.pixelSpacingY;
    float   originX         = curPix.originX;
    float   originY         = curPix.originY;
    float   originZ         = curPix.originZ;
    
    int colorDepth          = 32;
    
    NSMutableData   *newVolumeData     = [[NSMutableData alloc] initWithLength:0];
    NSMutableArray  *newPixList        = [[NSMutableArray alloc] initWithCapacity:0];
    
    long volumeSize     = pixWidth * pixHeight * nSlices * 4; // 4 Byte = 32 Bit Farbwert
    float *fVolumePtr   = (float *) malloc(volumeSize);
    if (!fVolumePtr){
        NSLog(@"ERROR: buffer(fVolumePtr) not allocated");
        return nil;
    }
    
    for( int i = 0; i < nSlices; i++ ){
        long imageSize = pixWidth * pixHeight * sizeof(float);
        float *imagePtr = (float *) malloc(imageSize);
        if (!imagePtr){
            NSLog(@"ERROR: buffer(imagePtr) not allocated");
            return nil;
        }
        memset(imagePtr,0.0,imageSize);
        
        DCMPix *emptyPix = [[DCMPix alloc] initWithData: imagePtr :colorDepth :pixWidth :pixHeight :pixelSpacingX :pixelSpacingY :originX :originY :originZ :true];
        
        emptyPix.sliceInterval = curPix.sliceInterval;
        emptyPix.sliceLocation = curPix.sliceLocation;
        emptyPix.sliceThickness = curPix.sliceThickness;
        double	orientation[9];
        [curPix orientationDouble: orientation];
        [emptyPix setOrientationDouble :orientation];
        
        [newPixList addObject: emptyPix];
        
        //free(imagePtr);
    }
    
    memset(fVolumePtr,0.0,volumeSize);
    newVolumeData = [[NSMutableData alloc] initWithBytesNoCopy:fVolumePtr length:volumeSize freeWhenDone:YES];
    
    
    NSMutableArray *newFileArray = [NSMutableArray arrayWithArray:[[viewerController fileList] subarrayWithRange:NSMakeRange(0,nSlices)]];
    ViewerController *new2DViewer = [ViewerController newWindow:newPixList :newFileArray :newVolumeData];
    
    if (new2DViewer)
        return new2DViewer;
    else
        return nil;
}

- (void) addMolliColormap{
    
    // basicly I copied the code from ViewerController.m -endCLUT
    NSMutableDictionary *clutDict		= [[[[NSUserDefaults standardUserDefaults] dictionaryForKey: @"CLUT"] mutableCopy] autorelease];
    if ([clutDict objectForKey:@"Molli_CLUT"]==nil){
        
        
        unsigned char red [] = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,2,5,7,11,16,19,21,24,27,30,32,35,38,41,43,46,49,52,55,57,60,63,66,68,70,75,80,82,84,87,91,93,95,98,101,104,107,109,112,115,118,120,123,126,129,131,137,140,142,145,148,151,154,156,157,159,160,161,162,163,165,166,168,169,170,171,172,173,176,178,179,180,181,183,184,185,186,188,189,190,191,193,194,195,196,198,199,201,202,203,205,207,208,209,211,212,213,214,216,217,218,219,221,222,223,224,226,227,227,229,232,234,235,235,237,239,240,240,242,244,244,245,247,249,249,251,253,253,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255};
        unsigned char green [] = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,3,6,11,13,16,18,21,23,25,28,31,33,35,38,41,43,45,48,51,53,55,57,62,66,68,70,72,76,78,80,82,86,88,90,92,96,98,100,102,105,108,109,111,113,115,117,119,121,123,124,126,127,129,131,132,134,136,137,139,140,142,144,146,147,149,151,153,156,157,159,160,162,164,166,167,169,170,172,174,175,177,179,181,182,183,184,187,191,192,193,194,197,199,200,201,203,204,207,208,209,211,214,215,216,218,219,221,223,225,227,229,231,233,235,236,237,239,241,243,244,246,248,249,251,252,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255};
        unsigned char blue [] = {0,29,35,39,43,47,51,55,58,62,67,71,74,78,83,86,90,94,98,102,106,111,118,122,125,129,134,138,141,145,149,153,157,161,165,169,172,174,177,181,184,187,189,194,199,203,205,208,211,214,217,220,224,227,231,231,230,229,229,228,227,226,225,224,221,217,215,211,207,204,201,198,195,191,187,185,181,178,174,171,168,164,161,158,154,150,145,140,138,134,131,128,124,121,117,114,111,107,104,101,98,95,92,87,84,81,78,73,68,64,61,58,54,51,48,45,41,38,35,31,28,25,21,18,15,11,8,5,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,5,12,19,26,33,41,48,55,62,69,76,83,90,102,112,119,127,134,141,148,155,163,169,176,184,191,198,205,212,220,226,232,240};
        
        
        NSMutableDictionary *aCLUTFilter	= [NSMutableDictionary dictionary];
        NSMutableArray		*rArray = [NSMutableArray array];
        NSMutableArray		*gArray = [NSMutableArray array];
        NSMutableArray		*bArray = [NSMutableArray array];
        for( int i = 0; i < 256; i++) [rArray addObject: [NSNumber numberWithLong: red  [i]]];
        for( int i = 0; i < 256; i++) [gArray addObject: [NSNumber numberWithLong: green[i]]];
        for( int i = 0; i < 256; i++) [bArray addObject: [NSNumber numberWithLong: blue [i]]];
        
        [aCLUTFilter setObject:rArray forKey:@"Red"];
        [aCLUTFilter setObject:gArray forKey:@"Green"];
        [aCLUTFilter setObject:bArray forKey:@"Blue"];
        
        [clutDict setObject: aCLUTFilter forKey: @"Molli_CLUT"];
        [[NSUserDefaults standardUserDefaults] setObject: clutDict forKey: @"CLUT"];
        
        [[NSNotificationCenter defaultCenter] postNotificationName: OsirixUpdateCLUTMenuNotification object: [viewerController curCLUTMenu] userInfo: [NSDictionary dictionary]];
    }
}

- (void) addShmolliColormap{
    
    // basicly I copied the code from ViewerController.m -endCLUT
    NSMutableDictionary *clutDict		= [[[[NSUserDefaults standardUserDefaults] dictionaryForKey: @"CLUT"] mutableCopy] autorelease];
    
    if ([clutDict objectForKey:@"Sholli_CLUT"]==nil){
        
        unsigned char red [] = {0,144,126,107,89,71,55,44,34,24,14,4,0,0,0,0,0,0,0,1,1,1,2,2,2,3,3,3,3,4,4,4,4,5,5,5,5,6,6,6,6,7,7,7,7,7,7,6,6,5,5,4,3,3,2,2,1,1,0,0,0,0,7,17,28,38,48,59,69,80,90,100,111,121,132,142,152,163,171,174,178,181,184,187,190,193,196,200,203,206,209,212,215,218,222,224,226,228,229,231,233,235,237,239,241,243,244,246,248,250,252,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,253,252,251,250,249,247,246,245,244,243,242,241,240,239,238,237,236,235,233,232,231,230,229,228,227,226,225,224,223,222,221,219,218,217,216,215,214,213,212,211,210,209,208,207,205,204,203,202,201,200,199,198,197,196,195,194,193,192,190,189,188,187,186,185,184,183,182,181,180,179,178,176};
        unsigned char green [] = {0,184,164,143,122,101,82,67,52,37,21,6,3,11,20,28,37,45,53,62,70,79,87,96,104,113,121,130,138,144,149,155,160,166,171,177,183,188,194,199,205,210,216,221,227,231,232,234,235,237,238,239,241,242,244,245,246,248,249,250,252,253,252,251,250,248,247,245,244,243,241,240,238,237,236,234,233,232,229,224,218,212,207,201,196,190,185,179,174,168,162,157,151,146,140,133,124,116,107,99,90,82,74,65,57,48,40,31,23,14,6,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,3,3,3,3,3,3,3,3,3,3,3,3,3,3};
        unsigned char blue [] = {0,248,249,250,251,253,254,254,254,254,254,254,253,251,249,247,245,243,241,239,238,236,234,232,230,228,226,224,223,220,216,213,210,207,204,201,198,194,191,188,185,182,179,176,172,167,156,146,135,125,115,104,94,84,73,63,52,42,32,21,11,0,0,0,1,1,2,3,4,5,5,6,7,8,9,9,10,11,11,11,10,10,9,9,8,8,8,7,7,6,6,5,5,4,4,3,3,3,2,2,2,1,1,1,1,0,0,0,0,0,0,0,0,1,2,3,4,5,6,7,9,10,11,12,13,14,15,16,17,19,20,21,22,23,24,25,26,27,28,30,31,32,33,34,35,36,37,38,39,41,42,43,44,45,46,47,48,49,51,52,53,54,55,56,57,58,59,60,62,63,64,65,66,67,68,69,70,71,73,74,75,76,77,78,80,82,85,87,90,92,94,97,99,101,104,106,109,111,113,116,118,121,123,125,128,130,132,135,137,140,142,144,147,149,152,154,156,159,161,164,166,168,171,173,175,178,180,183,185,187,190,192,195,197,199,202,204,206,209,211,214,216,218,221,223,226,228,230,233,235,237,240,242,245,247,249};
        
        NSMutableDictionary *aCLUTFilter	= [NSMutableDictionary dictionary];
        NSMutableArray		*rArray = [NSMutableArray array];
        NSMutableArray		*gArray = [NSMutableArray array];
        NSMutableArray		*bArray = [NSMutableArray array];
        for( int i = 0; i < 256; i++) [rArray addObject: [NSNumber numberWithLong: red  [i]]];
        for( int i = 0; i < 256; i++) [gArray addObject: [NSNumber numberWithLong: green[i]]];
        for( int i = 0; i < 256; i++) [bArray addObject: [NSNumber numberWithLong: blue [i]]];
        
        [aCLUTFilter setObject:rArray forKey:@"Red"];
        [aCLUTFilter setObject:gArray forKey:@"Green"];
        [aCLUTFilter setObject:bArray forKey:@"Blue"];
        
        [clutDict setObject: aCLUTFilter forKey: @"ShMolli_CLUT"];
        [[NSUserDefaults standardUserDefaults] setObject: clutDict forKey: @"CLUT"];
        
        [[NSNotificationCenter defaultCenter] postNotificationName: OsirixUpdateCLUTMenuNotification object: [viewerController curCLUTMenu] userInfo: [NSDictionary dictionary]];
    }
}

@end
