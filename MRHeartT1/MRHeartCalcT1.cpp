//
//  MRHeartCalcT1.cpp
//  MRHeartT1
//
//  Created by Konrad on 28/11/15.
//
//

#include "MRHeartCalcT1.h"

static int NImages;

void myT1(vnl_vector<double> &results, vnl_vector<double> &invTimes, vnl_vector<double> &ysignal, vnl_vector<double> const&params){
    double A  = params[0];
    double B  = params[1];
    double T1 = params[2];
    
    results = A - B * (-invTimes/T1).apply(exp);
}

void myT1residuals(vnl_vector<double> &residuals, vnl_vector<double> &invTimes, vnl_vector<double> &ysignal, vnl_vector<double> const&params) {
    
    vnl_vector<double> results;
    myT1(results, invTimes, ysignal, params);
    residuals = results - ysignal;
}

void myT1calcFunGrad(vnl_vector<double> &gradient, vnl_vector<double> &invTimes, vnl_vector<double> &ysignal, vnl_vector<double> const&params) {
    
    double A  = params[0];
    double B  = params[1];
    double T1 = params[2];
    
    // double check it!!!
    vnl_vector< double > myexp;
    myexp = (-invTimes/T1).apply(exp);
    vnl_vector< double > myexp_invTimes;
    myexp_invTimes = element_product(myexp,invTimes);
    
    //calculated in matlab (syms A B T1 t y), f=(A-B*exp(-t./T1)-y).^2; diff(f,A), diff(f,B), diff(f,T1)
    gradient[0]=(A*2 - ysignal*2 - myexp*B*2).sum();
    gradient[1]=dot_product(
                            myexp*2,
                            (ysignal - A + myexp*B));
    gradient[2]=dot_product(
                            myexp_invTimes*B*2,
                            ysignal - A + myexp*B)/T1/T1;
}

struct my_T1_functorLS : public vnl_least_squares_function {
    
    vnl_vector<double> invTimes;
    vnl_vector<double> ysignal;
    
    void f(vnl_vector<double> const& params, vnl_vector<double> &residuals)
    {
        myT1residuals(residuals,invTimes,ysignal,params);
    }
    my_T1_functorLS():vnl_least_squares_function (3,NImages,no_gradient){}
};

struct my_T1_functorCost : public vnl_cost_function {
    
    vnl_vector<double> invTimes;
    vnl_vector<double> ysignal;
    
    double f(vnl_vector<double> const& params)
    {
        vnl_vector<double> residuals;
        myT1residuals(residuals,invTimes,ysignal,params);
        
        //sum of squares of residuals
        return ((residuals).apply(vnl_math::sqr)).sum();
    }
    
    void gradf (vnl_vector< double > const &params, vnl_vector< double > &gradient){
        myT1calcFunGrad(gradient,invTimes,ysignal,params);
    }
    
    my_T1_functorCost():vnl_cost_function(3){}
};

double calcR2(vnl_vector<double> &invTimes, vnl_vector<double> &ysignal, vnl_vector<double> const&params){
    vnl_vector<double> residuals;
    myT1residuals(residuals,invTimes,ysignal,params);
    double SStot = element_product(
                                   (ysignal-ysignal.mean()),
                                   (ysignal-ysignal.mean())
                                   ).sum();
    double SSres = element_product(
                                   residuals,
                                   residuals).sum();
    
    if (SStot == 0)
        return 0;
    else
        return 1-SSres/SStot;
}

// sum {(O-E)^2/sigma2}
// O - observed
// E - expected
// sigma2 - variance
double calcChi2var(vnl_vector<double> &invTimes, vnl_vector<double> &ysignal, vnl_vector<double> const&params){
    vnl_vector<double> residuals;
    
    myT1residuals(residuals,invTimes,ysignal,params);
    
    // calc variance sigma2 = sum((y-mean(y))^2)/n, n-sample size
    double sigma2 = dot_product(
                                (ysignal-ysignal.mean()),
                                (ysignal-ysignal.mean())
                                )/ysignal.size();
    
    if (sigma2 == 0)
        return 0;
    else
        return dot_product(residuals,residuals)/sigma2;
}

// sum {(O-E)^2/E}
// O - observed
// E - expected
double calcChi2e(vnl_vector<double> &invTimes, vnl_vector<double> &ysignal, vnl_vector<double> const&params){
    vnl_vector<double> expected;
    vnl_vector<double> denominator;
    
    myT1(expected,invTimes,ysignal,params);
    denominator = element_product(ysignal-expected, ysignal-expected);
    
    return (element_quotient(denominator,expected)).sum();
}

void myT1calcPolarity(vnl_vector<double> &invTimes, vnl_vector<double> &ysignal, float &A, float &B, float &T1star, float &R2){
    NImages = invTimes.size();
    
    vnl_vector<double> pInit(3);
    pInit[0] = 200;
    pInit[1] = 300;
    pInit[2] = 1500;
    
    vnl_vector<double> As(NImages), Bs(NImages), T1s(NImages), ysignal_temp(NImages);
    //vnl_vector<double> chi2es(NImages);
    //vnl_vector<double> chi2vars(NImages);
    vnl_vector<double> R2s(NImages);
    vnl_vector<double> p_temp(3);
    
    my_T1_functorLS funLS;
    funLS.invTimes = invTimes;
    vnl_levenberg_marquardt LMminimizer(funLS);
    
//    my_T1_functorCost funCost;
//    funCost.invTimes = invTimes;
//    vnl_amoeba AMminimizer(funCost);
    
    for (int n = 0; n < NImages; n++){
        p_temp = pInit;
        ysignal_temp = ysignal;
        
        // LS minimalization
        funLS.ysignal = ysignal_temp.update(-ysignal_temp.extract(n,0),0);
        LMminimizer.minimize(p_temp);
        
        //AO minimalization
//        funCost.ysignal = ysignal_temp.update(-ysignal_temp.extract(n,0),0);
//        AMminimizer.minimize(p_temp);
        
        As[n]  = p_temp[0];
        Bs[n]  = p_temp[1];
        T1s[n] = p_temp[2];
        R2s[n] = calcR2(invTimes, ysignal_temp, p_temp);
        //chi2es[n] = calcChi2e(invTimes, ysignal_temp, p_temp);
        //chi2vars[n] = calcChi2var(invTimes, ysignal_temp, p_temp);
    }
    
    //int idx = chi2vars.arg_min();
    int idx = R2s.arg_max();
    
    A = (float)As[idx];
    B = (float)Bs[idx];
    T1star = (float)T1s[idx];
    R2 = (float)(R2s[idx]);
}

// threading
void myT1calcPolarityImage(vnl_vector<double> &invTimes, int width, int height, float **images,
                           float *AImagePtr, float *BImagePtr, float *T1starImagePtr, float *R2ImagePtr, float *T1ImagePtr){
    unsigned concurentThreadsSupported = std::thread::hardware_concurrency();
    vcl_cout << "Number of threads: " << concurentThreadsSupported << vcl_endl;
    
    std::vector<std::thread> threads;
    std::vector<int> limits = bounds(concurentThreadsSupported, width*height);
    
    for (int i = 0; i < concurentThreadsSupported; ++i){
        threads.push_back(std::thread(myT1calcPolarityThread, std::ref(invTimes), limits[i], limits[i+1], images, AImagePtr, BImagePtr, T1starImagePtr, R2ImagePtr, T1ImagePtr));
    }
    
    for(auto &t : threads){
        t.join();
    }
}

void myT1calcPolarityThread(vnl_vector<double> &invTimes, int posStart, int posStop, float **images,
                            float *AImagePtr, float *BImagePtr, float *T1starImagePtr, float *R2ImagePtr, float *T1ImagePtr){
    
    NImages = invTimes.size();
    vnl_vector<double> ysignal(NImages);
    
    for (int i=posStart; i<posStop; i++){
        for (int iImage=0; iImage<NImages; iImage++){
            ysignal[iImage] = (double)images[iImage][i];
        }
        myT1calcPolarity(invTimes,ysignal,AImagePtr[i],BImagePtr[i],T1starImagePtr[i],R2ImagePtr[i]);
        
        T1ImagePtr[i] = T1starImagePtr[i]*(BImagePtr[i]/AImagePtr[i]-1);
        
        if      (AImagePtr[i] > 4096)       { AImagePtr[i] = 4096;}
        else if (AImagePtr[i] < 0)          { AImagePtr[i] = 0; }
        if      (BImagePtr[i] > 4096)       { BImagePtr[i] = 4096;}
        else if (BImagePtr[i] < 0)          { BImagePtr[i] = 0; }
        if      (T1starImagePtr[i] > 4096)  { T1starImagePtr[i] = 4096;}
        else if (T1starImagePtr[i] < 0)     { T1starImagePtr[i] = 0; }
        if      (T1ImagePtr[i] > 4096)      { T1ImagePtr[i] = 4096;}
        else if (T1ImagePtr[i] < 0)         { T1ImagePtr[i] = 0; }
        
        R2ImagePtr[i] *= 4000;
    }
}

// from https://solarianprogrammer.com/2012/02/27/cpp-11-thread-tutorial-part-2/
std::vector<int> bounds(int parts, int mem) {
    std::vector<int>bnd;
    int delta = mem / parts;
    int reminder = mem % parts;
    int N1 = 0, N2 = 0;
    bnd.push_back(N1);
    for (int i = 0; i < parts; ++i) {
        N2 = N1 + delta;
        if (i == parts - 1)
            N2 += reminder;
        bnd.push_back(N2);
        N1 = N2;
    }
    return bnd;
}

