//
//  MRHeartT1Filter.h
//  MRHeartT1
//
//  Copyright (c) 2015 Konrad_Werys. All rights reserved.
//

#import <Foundation/Foundation.h>
#include <Accelerate/Accelerate.h>

#import <OsiriXAPI/AppController.h>
#import <OsiriXAPI/BrowserController.h>
#import <OsiriXAPI/DicomStudy.h>
#import <OsiriXAPI/DicomSeries.h>
#import <OsiriXAPI/DicomImage.h>
#import <OsiriXAPI/Notifications.h>
#import <OsiriXAPI/PluginFilter.h>

#include <vnl/vnl_vector.h>
#include <vnl_index_sort.h>

#import "DCMObject.h"
#import "DCMAttribute.h"
#import "DCMAttributeTag.h"

#include <sys/time.h>

@interface MRHeartT1Filter : PluginFilter {
}

- (long) filterImage:(NSString*) menuName;
- (ViewerController*) newViewerControlerSimilarTo: (DCMPix*) curPix withNSlices: (int) nSlices;

@end
