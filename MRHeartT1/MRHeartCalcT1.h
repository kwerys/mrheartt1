//
//  MRHeartCalcT1.hpp
//  MRHeartT1
//
//  Created by Konrad on 28/11/15.
//
//

//#ifndef MRHeartCalcT1_hpp
//#define MRHeartCalcT1_hpp
//
//#include <stdio.h>
//
//#endif /* MRHeartCalcT1_hpp */

#include <vcl_iostream.h>
#include <vnl/vnl_matrix.h>
#include <vnl/vnl_vector.h>
#include <vnl/vnl_math.h>
#include <vnl/vnl_least_squares_function.h>
#include <vnl/vnl_cost_function.h>
#include <vnl/algo/vnl_levenberg_marquardt.h>
#include <vnl/algo/vnl_amoeba.h>
#include <vnl/algo/vnl_conjugate_gradient.h>
#include <vnl/algo/vnl_powell.h>
#include <vnl/algo/vnl_lbfgs.h>
#include <vnl/vnl_index_sort.h>

#include <vector>
#include <thread>

std::vector<int> bounds(int parts, int mem);
void myT1calcPolarity(vnl_vector<double> &invTimes, vnl_vector<double> &ysignal,
                      float &A, float &B, float &T1star, float &R2);
void myT1calcPolarityImage(vnl_vector<double> &invTimes, int width, int height, float **images,
                           float *AImagePtr, float *BImagePtr, float *T1starImagePtr, float *R2ImagePtr, float *T1ImagePtr);
void myT1calcPolarityThread(vnl_vector<double> &invTimes, int posStart, int posStop, float **images,
                            float *AImagePtr, float *BImagePtr, float *T1starImagePtr, float *R2ImagePtr, float *T1ImagePtr);
void myT1calcFun(vnl_vector<double> &result, vnl_vector<double> &TIs, vnl_vector<double> &ysignal, double A, double B, double T1);